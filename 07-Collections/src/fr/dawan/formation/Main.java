package fr.dawan.formation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {
        // Avant java SE 5 => on stocker tous objets qui héritent de Object
        List lst1 = new ArrayList(); // List => interface , ArrayList =>objet "réel" qui implémente l'interface
        lst1.add("AZERTY"); // add => ajouter un objet à la collection
        lst1.add(123);
        lst1.add("HELLO");
        lst1.add(3.5);
        lst1.remove(1);
        if (lst1.get(0) instanceof String) {    // get =>   récupérer un objet stocké dans la liste à l'indice 0
            String str = (String) lst1.get(0);  //          retourne des objects, il faut tester si l'objet correspont à la classe et le caster
        }

        // Parcourir une collection avec Iterateur
        Iterator iter = lst1.iterator();
        while (iter.hasNext()) {
            System.out.println(iter.next());
        }

        // Java 5 => les types générique
        List<String> lst2 = new ArrayList<>();  // la liste ne peut plus contenir que des chaines de caractères
        lst2.add("HELLO");
        lst2.add("WORLD");
        lst2.add(0,"azerty");    // insertion à l'index 0 uniquement pour les listes
        lst2.add("asupprimeri");
        lst2.add("asupprimero");
        System.out.println(lst2.size()); // size => le nombre d'élément de la collection
        System.out.println(lst2.set(0, "AZERTY")); // set => remplace l'élément à l'index par celui passé en paramètre

        System.out.println(lst2.get(1)); // get => revoie l'élément placé à l'index

        lst2.remove(4);                 // supprimer l'élément à l'index 4
        lst2.remove("asupprimero");     // supprime le premier élément "asupprimero"
        // Parcourir une collection "foreach"
        for (String str : lst2) {
            System.out.println(str);
        }

        // Set => collection qui ne contient pas de doublons
        Set<Double> st1 = new HashSet<>();
        st1.add(12.5);
        System.out.println(st1.add(23.78));
        System.out.println(st1.add(12.5)); // add retourne false 12.5 existe déjà dans la collection
        st1.add(56.0);
        st1.add(92.66);
        for (double d : st1) {
            System.out.println(d);
        }
        
//      SortedSet =>  tous les objets sont automatiquement triés 
//
//      Set trié => interface Comparator
//      Classe anonyme
//       SortedSet<String>st2 =new TreeSet<>(new Comparator<String>() {
//
//        @Override
//        public int compare(String o1, String o2) {
//            return -o1.compareTo(o2);
//        }
//    });

        SortedSet<String> st2 = new TreeSet<>(new StringComparator());
        st2.add("HELLO");
        st2.add("aZERTY");
        st2.add("AZERTY");
        st2.add("World");
        st2.add("Bonjour");
        System.out.println(st2.first());
        for (String s : st2) {
            System.out.println(s);
        }

        // Set trié => interface Comparable
        SortedSet<User> stUser = new TreeSet<>();
        stUser.add(new User("Alan", "Smithee"));
        stUser.add(new User("John", "Doe"));
        stUser.add(new User("Marcel", "Dupont"));
        stUser.add(new User("Jo", "Dalton"));
        for (User u : stUser) {
            System.out.println(u);
        }
        
        //Queue => file d'attente simple
        Queue<Integer> pileFifo = new LinkedList<>();
        pileFifo.offer(23);
        pileFifo.offer(45);
        pileFifo.offer(78);
        System.out.println(pileFifo.remove());
        System.out.println(pileFifo.peek());
        System.out.println(pileFifo.remove());
        System.out.println(pileFifo.remove());
        System.out.println(pileFifo.poll());
        
        // Map => association Clé/Valeur 
        Map<String, User > m=new HashMap<>();
        m.put("fr59-1", new User("Alan", "Smithee")); // put => ajouter la clé "fr59-1" et la valeur associé (objet User)
        m.put("fr59-2", new  User("John", "Doe"));
        m.put("fr59-3", new User("Marcel", "Dupont"));
        m.put("fr59-4", new  User("Jo", "Dalton"));
        
        System.out.println(m.get("fr59-2"));        // Obtenir la valeur associé à la clé fr59-2
        System.out.println(m.containsKey("nonpresent")); // false,la clé nonpresent n'est pas présente dans la map
        System.out.println(m.containsValue( new User("John", "Doe"))); // true,la valeur est pas présente dans la map
        
        m.put("fr59-3", new User("Marcel", "Dupond")); //si existe déjà, elle est écrasée par la nouvelle valeur associé à la clé 
        
        Set<String> keys=m.keySet();    // keySet => retourne toutes les clés de la map
        for(String k:keys) {
            System.out.println(k);
        }
        
        Collection<User> vals=m.values(); // values => retourne toutes les valeurs de la map
        for(User u: vals) {
            System.out.println(u);
        }
        
        // Entry => objet qui contient une clé et la valeur associée 
        Set<Entry<String, User>> ent= m.entrySet();
        for(Entry<String, User> e:ent) {
            System.out.println(e.getKey());
            System.out.println(e.getValue());
        }
        
        // Classe Collections => Classe utilitaires pour les collections
        System.out.println(Collections.min(lst2)); // min=> l'élément minimum de la collection 
        Collections.sort(lst2); // sort => pour trier une liste
        for(String s: lst2) {
            System.out.println(s);
        }
        
        // Classe Arrays  => Classe utilitaires pour les tableaux
        String [] tab=new String[5] ;
        Arrays.fill(tab, "Hello");      // => initialiser un tableau avec une valeur
        
        System.out.println(Arrays.toString(tab));   // toString =>  afficher un tableau 
        
        int t1[]= {1,2,3,4,5};
        int t2[]= {1,2,3,4,5};
        int t3[]= {3,7,4,10,5,1};
        System.out.println(Arrays.equals(t1, t2));  // equals => comparaison de deux tableaux
        System.out.println(Arrays.equals(t1, t3));
        
        Arrays.sort(t3);                            // sort => tri d'un tableau
        System.out.println(Arrays.toString(t3));
        
        System.out.println(Arrays.binarySearch(t3, 10));            // binarySearch => recherche d'un élément dans un tableau trié
        System.out.println(Arrays.binarySearch(t3, 6)); // (-5 +1 ) x -1= 4
        int t2d[][]= {{6,1,2,4},{7,3,10,1}};
        Arrays.sort(t2d[0]);
        
        System.out.println(Arrays.toString(t2d[0]));
        System.out.println(Arrays.toString(t2d[1]));
        
        String[] tabl2=lst2.toArray(new String[0]);
        System.out.println(Arrays.toString(tabl2));
        
        String[]  tabl21=new String[lst2.size()];
        lst2.toArray(tabl21);
        System.out.println(Arrays.toString(tabl21));
    }

}
