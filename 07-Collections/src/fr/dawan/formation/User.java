package fr.dawan.formation;

//Pour le tri avec sortedset la classe User implémente l'interface Comparable
public class User implements Comparable<User> {

    private String prenom;
    private String nom;

    public User() {
    }

    public User(String prenom, String nom) {
        this.prenom = prenom;
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((nom == null) ? 0 : nom.hashCode());
        result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (nom == null) {
            if (other.nom != null)
                return false;
        } else if (!nom.equals(other.nom))
            return false;
        if (prenom == null) {
            if (other.prenom != null)
                return false;
        } else if (!prenom.equals(other.prenom))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "User [prenom=" + prenom + ", nom=" + nom + "]";
    }

    @Override
    public int compareTo(User o) {
        if (nom.compareToIgnoreCase(o.nom) > 0) {
            return -1;
        } else if (nom.compareToIgnoreCase(o.nom) < 0) {
            return 1;
        } else {
            return 0;
        }
    }

}
