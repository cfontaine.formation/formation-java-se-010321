package fr.dawan.formation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.security.Policy.Parameters;

public class Main {

    public static void main(String[] args) {
        // Class => Une classe qui représente une classe java
        // 1- Obtenir Class avec .class sur le nom de la Classe
        Class c = Inconnu.class;
        // 2- Obtenir Class avec la méthode getClass
        Inconnu i = new Inconnu();
        Class c2 = i.getClass();
        // 3- Obtenir Class à partir de son nom complet avec la méthode forName de Class
        try {
            Class c3 = Class.forName("fr.dawan.formation.Inconnu");
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        }
        // getName => retourne le nom de la classe
        System.out.println(c2.getName());

        // getSuperclass => retourne l'objet Class de la classe mère
        System.out.println(c2.getSuperclass().getName());

        // getFields => retourne les attributs public
        Field[] attributs = c2.getFields();
        System.out.println("Attributs public:");
        for (Field f : attributs) {
            System.out.println("\t" + f.getName());
        }
        System.out.println("__________________");
        System.out.println("Méthodes public:");

        // getMethods => retourne les méthodes public
        Method[] meths = c2.getMethods();
        for (Method m : meths) {
            System.out.println("\t" + m.getName());
        }
        System.out.println("__________________");
        System.out.println("Constructeurs public:");

        // getConstructors => retourne les constructeurs public
        Constructor[] constructs = c2.getConstructors();
        for (Constructor ct : constructs) {
            System.out.println("\t" + ct.getName() + " nombre de paramètre=" + ct.getParameterCount());
            Parameter[] params = ct.getParameters();
            for (Parameter par : params) {
                System.out.println("\t\t" + par.getName() + " " + par.getType());
            }
        }
        System.out.println("__________________");

        try {
            // création dynamique des instances d'un type
            Inconnu inc = (Inconnu) c2.getConstructor().newInstance();
            inc.methode1();
            Inconnu inc2 = (Inconnu) c2.getConstructor(String.class).newInstance("azedvejv");
            inc2.methode1();
            // excecution dynamique des méthodes
            Method[] me = c2.getMethods();
            System.out.println("__________________");
            if (me.length > 0) {
                me[0].invoke(inc);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        }

    }

}
