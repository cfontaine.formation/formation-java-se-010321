/**
 * La classe Helloworld
 * @author Christophe Fontaine
 *
 */
public class HelloWorld {
    
    /**
     * Point d'entrée du programme
     * @param args arguments de la ligne de commande
     */
    public static void main(String[] args) {    // Commentaire fin de ligne
       
        /* Commentaire sur
         * plusieurs lignes
         */
        System.out.println("Hello World !!!");
    }

}
