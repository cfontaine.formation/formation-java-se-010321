package fr.dawan.formation;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.StringTokenizer;

public class Main {

    public static void main(String[] args) {
        
        // Wrapper
        Integer iW = new Integer(1);
        System.out.println(iW);

        // Convertion String en type primitif
        int iStr = Integer.parseInt("100");
        System.out.println(iStr);

        // Convertion String vers Wrapper
        Integer iwStr = Integer.valueOf("100");
        System.out.println(iwStr);

        // Affichage d'un nombre au format Hexadécimal
        System.out.println(Integer.toHexString(iStr));
        // Affichage d'un nombre au format binaire
        System.out.println(Integer.toBinaryString(iStr));

        // Conversion Integer vers int
        int ii = iwStr.intValue();
        // Conversion Integer vers double
        double id = iwStr.doubleValue();
        System.out.println(ii + " " + id);

        // Autoboxing => Convertion automatique du type primitif vers un objet (Wrapper)
        int i = 34;
        iW = i;
        System.out.println(i + " " + iW);

        // Unboxing => Convertion automatique d'un objet (wrapper) vers un type primitif
        i = iW;
        System.out.println(i + " " + iW);

        // Chaine de caractère
        String s1 = "test";     // Une chaine litérale est stockée dans le stringpool
        String s2 = "test";     // Il n'y a qu'une seule objet String qui contient azerty
        String s3 = new String("test"); // une chaine de caractère créé avec le constructeur est stocké dans le heap
        String s4 = new String("test"); // Il y a 2 objets créé en mémoire
        System.out.println((s1 == s2) + "   " + (s3 == s4));
        // true, s1 et s2 pointe vers un seul objet
        // false s3 et s4 ne sont pas égal, il pointe sur 2 objets différents
        System.out.println((s1.equals(s2)) + "   " + (s3.equals(s4)));  // true equals compare 2 chaines de caractère

        String str = "Hello World";
        String str2 = "Bonjour";
        
        System.out.println(str.length());   // length -> Nombre de caractère de la chaine de caractère
        
        // Comparaison 0-> égale, >0 -> se trouve après, <0 -> se trouve avant dans l'ordre alphabétique
        System.out.println(str.compareTo(str2));
        System.out.println(str2.compareTo(str));

        // Concaténation
        System.out.println(str + str2);
        System.out.println(str.concat(str2));
        System.out.println(str);    // les chaines de caractères sont immuables une fois créée elles ne peuvent plus être modifiées
      
        // La méthode join concatène les chaines, en les séparants par une chaine de séparation
        String j = String.join(";", "azerty", "sdfgh", "dsfsfgdg");
        System.out.println(j);  // "azerty;sdfgh;dsfsfgdg"

        // Découpe la chaine en un tableau de sous-chaine suivant un séparateur
        String[] tabStr = j.split(";");
        for (String s : tabStr) {
            System.out.println(s);
        }

        // substring permet d'extraire une sous-chaine
        // de l'indice passé en paramètre jusqu'à la fin de la chaine
        System.out.println(str.substring(6));   // World
        // de l'indice passé en paramètre jusqu'à l'indice de fin (exclut)
        System.out.println(str.substring(6, 9));   // Wor

        // startsWith retourne true, si la chaine commence par la chaine passé en paramètre
        System.out.println(str.startsWith("Hello")); // true
        System.out.println(str.startsWith("bonjour")); // false

        // indexOf retourne la première occurence de la chaine passée en paramètre
        System.out.println(str.indexOf("o"));
        System.out.println(str.indexOf("o", 6));    // idem mais à partir de l'indice passé en paramètre
        System.out.println(str.indexOf("o", 8));    // retourne -1, si le caractère n'est pas trouvé 
       
        // Remplace toutes les les sous-chaines target par replacement
        System.out.println(str.replace("o", "a"));  // Hella Warld
       
        // Retourne true, si la sous-chaine passée en paramètre est contenu dans la chaine
        System.out.println(str.contains("Wo"));
        
        // Retourne le caractère à l'indice 4
        System.out.println(str.charAt(4));
        
        // Retourne la chaine en minuscule
        System.out.println(str.toLowerCase());
        
        // Retourne la chaine en majuscule
        System.out.println(str.toUpperCase());
        
        // trim supprime les caractères de blanc du début et de la fin de la chaine
        System.out.println("   \n \t cvjvd  hkhkhkh \n \t   ".trim());
        
        // Retourne une chaine formater
        System.out.println(String.format("%d  [%s]", 10, "Bonjour"));

        // StringBuilder
        // Lorsqu'il y a beaucoup de manipulation (+de 3) sur une chaine ( concaténation et insertion, remplacement,supression de sous-chaine)
        // il vaut mieux utiliser un objet StringBuilder qui est plus performant qu'un objet String => pas de création d'objet intermédiare
        StringBuilder sb = new StringBuilder("Hello");
        sb.append(false);
        sb.append("Test");
        String str3 = sb.toString();
        System.out.println(str3);

        // StringTokenizer
        // Permet de décomposer une chaîne de caractères en une suite de mots séparés par des délimiteurs
        StringTokenizer tok = new StringTokenizer("azerty;sdfgh;dsfsfgdg", ";");
        while (tok.hasMoreTokens()) {   // retourne true tant qu'il reste des éléments à extraire
            System.out.println(tok.nextToken()); // renvoie l'élément suivant
        }

        // Date
        Date d = new Date();
        System.out.println(d);
        
        LocalTime heure = LocalTime.now();
        System.out.println(heure);
        LocalDate dateDuJour = LocalDate.now();
        System.out.println(dateDuJour);

        LocalDate noel2012 = LocalDate.of(2012, Month.DECEMBER, 25);
        System.out.println(noel2012);
        LocalDate jdl2017 = noel2012.plusYears(4).plusDays(7);
        System.out.println(noel2012);
        System.out.println(jdl2017);
        Period per = Period.ofYears(2);
        LocalDate jdl2019 = jdl2017.plus(per);

        System.out.println(dateDuJour.format(DateTimeFormatter.ISO_LOCAL_DATE));
        System.out.println(dateDuJour.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)));
        System.out.println(dateDuJour.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
        System.out.println(dateDuJour.format(DateTimeFormatter.ofPattern("dd/MM/YY")));

        Period per2 = Period.between(jdl2017, jdl2019);
        System.out.println(per2.getYears());
    }

}
