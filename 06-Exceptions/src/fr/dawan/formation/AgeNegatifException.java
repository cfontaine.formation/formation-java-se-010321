package fr.dawan.formation;

//Créer sa propre exception => hériter de Exception ou d'une de ses sous-classes
public class AgeNegatifException extends Exception {

    private static final long serialVersionUID = 1L;

    public AgeNegatifException(int age) {
        super("L'age est négatif : " + age);
    }

}
