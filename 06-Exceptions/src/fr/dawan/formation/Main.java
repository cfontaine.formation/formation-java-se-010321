package fr.dawan.formation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Main {

    @SuppressWarnings({ "resource", "unused" }) // => pour supprimer les warning
    public static void main(String[] args) {

        // checked exception => on est obligé (par java) de traiter l'exception
        try {
            // .....
            FileInputStream fo = new FileInputStream("nexistepas");  // ouverture d'un fichier qui n'existe pas
            int a = fo.read();
            System.out.println("Suite du code");
        } catch (FileNotFoundException e) {                          // si une exception FileNotFoundException ce produit dans le bloc try
            System.out.println("Le Fichier n'existe pas");
            return;
        } catch (IOException | NullPointerException e) {        // pour un bloc try, il peut y avoir plusieurs blocs catch pour traiter diférentes exceptions
            System.out.println("Ecriture Impossible");          // de la - à la + générale
        } catch (Exception e) {
            System.out.println("Une Erreur est survenue");
        } finally {                                         // le bloc finally est toujours exécuté
            System.out.println("Libérer les ressources");
        }

        // Runtime Exception => on peut traiter l'exception ou pas (pas d'obligation)
        try {
            int[] tab = new int[5];
            tab[10] = 12;
        } catch (ArrayIndexOutOfBoundsException e) {
            // e.printStackTrace();
            System.err.println("Indice en dehors du tableau :" + e.getMessage());
        }

        Scanner sc = new Scanner(System.in);
        int age = sc.nextInt();
        try {
            traitementEmploye(age);
        } catch (Exception e) {
            // System.err.println(e.getMessage());
            e.printStackTrace();
        }
        System.out.println("Suite du programme");
        sc.close();
    }

    // throws indique que l'exception n'est pas traiter localement mais elle est envoyer dans la méthode appelante
    public static void traitementEmploye(int age) throws Exception {
        System.out.println("Traitement de l'employé");
        try {
            traitementAge(age);
        } catch (Exception e) {
            System.err.println("Traitement local de l'exception");
            // Relance d'une exception
            // throw e;     // relance de la même exception
            throw new Exception("erreur age", e);   // relance d'un autre type d'exception avec en paramètre l'exception originale comme cause
        }
        System.out.println("Suite Traitement de l'employé");
    }

    public static void traitementAge(int age) throws AgeNegatifException {
        System.out.println("Traitement");
        if (age < 0) {
            throw new AgeNegatifException(age);     // throw permet de lancer une exception
        }
        System.out.println("Suite du Traitement");
    }

}
