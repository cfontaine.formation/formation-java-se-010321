package fr.dawan.formation;

public class Chien extends Animal{ // La classe Chien hérite de la classe Animal et implémente l'interface PeutMarcher et Cloneable

    private String nom;

    public Chien() {
        super();
    }

    public Chien(double poid, int age, String nom) {
        super(poid, age);
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public void emettreSon() {  // Redéfinition obligatoire de la méthode abstraite emmettreUnSon de la classe Animal
        System.out.println(nom + " aboie");
    }


    public void courrir() {     // Impléméntation de la méthode marcher de l'interface PeutMarcher
        System.out.println(nom + " court");
    }


    public void marcher() {     // Impléméntation de la méthode courrir de l'interface  IPeutMarcher
        System.out.println(nom + " marches");
    }

    @Override
    public String toString() {
        return "Chien [nom=" + nom + ", poid=" + getPoid() + ", age=" + getAge() + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((nom == null) ? 0 : nom.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Chien other = (Chien) obj;
        if (nom == null) {
            if (other.nom != null)
                return false;
        } else if (!nom.equals(other.nom))
            return false;
        return true;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
