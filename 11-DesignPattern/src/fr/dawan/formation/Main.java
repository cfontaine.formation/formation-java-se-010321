package fr.dawan.formation;

public class Main {

    public static void main(String[] args) {
        Utilitaire.test();     
        
        Singleton s=Singleton.getInstance();
        System.out.println(s.getI());
    
        Animal a=FactoryAnimal.createAnimal(AnimalEnum.CHIEN);
        a.emettreSon();
    }

}
