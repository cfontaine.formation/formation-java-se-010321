package fr.dawan.formation;

public class Canard extends Animal  {  // On peut implémenter plusieurs d'interface

    @Override
    public void emettreSon() {
        System.out.println("Coin coin");

    }


    public void atterir() {
        System.out.println("Le canard atterit");

    }

    public void decoller() {
        System.out.println("Le canard décolle");

    }


    public void marcher() {
        System.out.println("Le canard marche");
    }


    public void courrir() {
        System.out.println("Le canard court");
    }

}
