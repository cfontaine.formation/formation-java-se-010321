
public class Main {

    public static void main(String[] args) {
//        MyRunnable run1=new MyRunnable();
//        Thread t1=new Thread(run1);
//        t1.start();
        MyThread th1 = new MyThread(100);
        th1.setName("T1");
        MyThread th2 = new MyThread(400);
        th2.setName("T2");
        // th2.setDaemon(true);
        th1.start();
//        try {
//            th1.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        th1.interrupt();
        th2.start();
    }

}
