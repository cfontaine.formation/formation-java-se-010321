
public class MyThread extends Thread {

    private int tsl;

    public MyThread(int tsl) {
        this.tsl = tsl;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println(getName() + " " + i);
            try {
                sleep(tsl);
            } catch (InterruptedException e) {
                e.printStackTrace();
                break;
            }
        }
    }

}
