import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class ThreadFlux {

    public static void main(String[] args) {
        PipedOutputStream po = new PipedOutputStream();
        PipedInputStream pi = new PipedInputStream();
        try {
            po.connect(pi);
            Thread t1 = new Thread() {

                @Override
                public void run() {
                    for (int i = 0; i < 100; i++) {
                        try {
                            po.write(i);
                            Thread.sleep(300);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }

            };

            Thread t2 = new Thread() {

                @Override
                public void run() {
                    while (true) {
                        try {
                            int c = pi.read();
                            System.out.println(c);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

            };

            t1.start();
            t2.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
