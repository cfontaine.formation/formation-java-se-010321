package fr.dawan.formation;

public enum Direction {
    NORD, EST, SUD, OUEST
}
