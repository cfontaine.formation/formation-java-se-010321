package fr.dawan.formation;

public abstract class Animal {// Animal est une classe abstraite, elle ne peut pas être instantiée elle même
                              // mais uniquement par l'intermédiaire de ses classes filles

    private double poid;

    private int age;

    public Animal() {
    }

    public Animal(double poid, int age) {
        this.poid = poid;
        this.age = age;
    }

    public abstract void emettreSon();  // Méthode abstraite qui doit obligatoirement redéfinit par les classes filles

    public double getPoid() {
        return poid;
    }

    public void setPoid(double poid) {
        this.poid = poid;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Animal [poid=" + poid + ", age=" + age + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + age;
        long temp;
        temp = Double.doubleToLongBits(poid);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Animal other = (Animal) obj;
        if (age != other.age)
            return false;
        if (Double.doubleToLongBits(poid) != Double.doubleToLongBits(other.poid))
            return false;
        return true;
    }

}
