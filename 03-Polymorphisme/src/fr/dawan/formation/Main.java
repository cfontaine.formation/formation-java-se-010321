package fr.dawan.formation;

public class Main {

    public static void main(String[] args) {
//        Animal a1 = new Animal(3000, 7);  // Impossible la classe est abstraite
//        a1.emettreSon();
//        System.out.println(a1.getPoid());

        Chien c1 = new Chien(4000, 4, "Laika");
        c1.courrir();
        c1.emettreSon();

        Animal a2 = new Chien(2000, 5, "Idefix"); // On peut créer une instance de Chien (classe fille) qui aura une référence Animal (classe mère)
                                                  // L'objet Chien sera vu comme un Animal, on ne pourra pas accèder aux méthodes propre au chien (nom,...)
        System.out.println(a2.getPoid() + "  " + a2.getAge());
        a2.emettreSon();                          // C'est la méthode de Chien qui sera appelée

        if (a2 instanceof Chien) {  // test si a2 est de "type" Chien
            Chien c2 = (Chien) a2;  // Pour passer d'une super-classe à une sous-classe, il faut le faire explicitement avec un cast
            c2.courrir();            // avec la référence c2 (de type Chien), on a bien accès à toutes les méthodes de la classe Chien
        }

        Animal[] tab = new Animal[4];
        tab[0] = new Chien(4000, 4, "Laika");
        tab[1] = new Chat(5000, 6, "Garfield", 7);
        tab[2] = new Chien(2000, 5, "Idefix");
        tab[3] = new Canard();

        for (Animal an : tab) {
            an.emettreSon();
        }

        PeutMarcher[] tab2 = new PeutMarcher[4]; // On peut utiliser une référence vers une interface pour référencer une classe qui l'implémente
        tab2[0] = new Chien(4000, 4, "Laika");   // L'objet chien n'est vu que comme un interface PeutMarcher, on ne peut utiliser que les méthode de PeutMarcher (marcher, courrir)
        tab2[1] = new Chat(5000, 6, "Garfield", 7);
        tab2[2] = new Chien(2000, 5, "Idefix");
        tab2[3] = new Canard();

        for (PeutMarcher an : tab2) {
            an.marcher();
        }

        // Object
        // toString
        System.out.println(c1);

        // equals
        Chien c2 = new Chien(4000, 4, "Laika");
        System.out.println(c1 == c2);       // false, on compare les références (c1 et c2 sont 2 objets, leurs références sont différente)  
        System.out.println(c1.equals(c2));  // true, on compare le contenu des 2 objets, si dans la classe Chien equals est redéfinie

        String a = "AZERTY";
        String b = "AZERTY";
        System.out.println(a.equals(b));

        // clone
        Chien c3 = null;
        try {
            c3 = (Chien) c1.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        System.out.println(c3);

        // Enumération
        Direction dir = Direction.NORD;
        System.out.println(dir.name());

        System.out.println(dir.ordinal());

        String str = "NORD";
        Direction dir2 = Direction.valueOf(Direction.class, str);
        System.out.println(dir2);

        Direction tabDir[] = Direction.values();
        for (Direction d : tabDir) {
            System.out.println(d);
        }
    }

}
