package fr.dawan.formation;

public class Chat extends Animal implements PeutMarcher {

    private String nom;

    private int nbVie = 9;

    public Chat() {
        super();
    }

    public Chat(double poid, int age, String nom, int nbVie) {
        super(poid, age);
        this.nom = nom;
        this.nbVie = nbVie;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getNbVie() {
        return nbVie;
    }

    public void setNbVie(int nbVie) {
        this.nbVie = nbVie;
    }

    @Override
    public void emettreSon() {
        System.out.println(nom + " miaule");
    }

    @Override
    public void courrir() {
        System.out.println(nom + " court");
    }

    @Override
    public void marcher() {
        System.out.println(nom + " marches");
    }

}
