package fr.dawan.formation;

public class Canard extends Animal implements PeutMarcher, PeutVoler {  // On peut implémenter plusieurs d'interface

    @Override
    public void emettreSon() {
        System.out.println("Coin coin");

    }

    @Override
    public void atterir() {
        System.out.println("Le canard atterit");

    }

    @Override
    public void decoller() {
        System.out.println("Le canard décolle");

    }

    @Override
    public void marcher() {
        System.out.println("Le canard marche");
    }

    @Override
    public void courrir() {
        System.out.println("Le canard court");
    }

}
