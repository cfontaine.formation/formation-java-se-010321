package fr.dawan.formation;

// Un interface ne définit que des méthodes qui deveront être obligatoirement implémenté dans la classe qui va impléménter l'interface
public interface PeutMarcher {
    void marcher();

    void courrir();
}
