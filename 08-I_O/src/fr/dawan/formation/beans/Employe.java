package fr.dawan.formation.beans;

import java.io.Serializable;
import java.time.LocalDate;

public class Employe implements Serializable {

    private static final long serialVersionUID = 1L;

    private String prenom;
    private String nom;
    private LocalDate dateNaissance;
    private double salaire;
    private Contrat typeContrat=Contrat.CDI;

    public Employe() {
    }

    public Employe(String prenom, String nom, LocalDate dateNaissance, double salaire, Contrat typeContrat) {
        this.prenom = prenom;
        this.nom = nom;
        this.dateNaissance = dateNaissance;
        this.salaire = salaire;
        this.typeContrat = typeContrat;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public double getSalaire() {
        return salaire;
    }

    public void setSalaire(double salaire) {
        this.salaire = salaire;
    }

    public Contrat getTypeContrat() {
        return typeContrat;
    }

    public void setTypeContrat(Contrat typeContrat) {
        this.typeContrat = typeContrat;
    }

    @Override
    public String toString() {
        return "Employe [prenom=" + prenom + ", nom=" + nom + ", dateNaissance=" + dateNaissance + ", salaire="
                + salaire + ", typeContrat=" + typeContrat + "]";
    }

}
