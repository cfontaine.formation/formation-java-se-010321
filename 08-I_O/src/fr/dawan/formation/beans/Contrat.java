package fr.dawan.formation.beans;

public enum Contrat {
    CDI, CDD, INTERIM, STAGE
}
