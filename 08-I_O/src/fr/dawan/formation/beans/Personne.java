package fr.dawan.formation.beans;

import java.io.Serializable;
import java.time.LocalDate;

//Pour qu'une classe soit sérialisable, elle doit implémenter l'interface Serializable
public class Personne implements Serializable {

    // serialVersionUID est une clé de hachage SHA qui identifie de manière unique la classe.
    // Si la classe personne évolue et n'est plus compatible avec les objets précédamment persistés, on modifie la valeur du serialVersionUID
    // et lors de la désérialisation une exception sera générée pour signaler l'incompatibilité
    // java.io.InvalidClassException: fr.dawan.formation.beans.Personne; local class incompatible: stream classdesc serialVersionUID = 1, local class serialVersionUID = 2
    // si l'on fournit pas serialVersionUID le compilateur va en générer un (à éviter).
    private static final long serialVersionUID = 1L;

    private String prenom;
    private String nom;
    private Adresse adresse;
    // Si un attribut ne doit pas être sérialiser on ajout le mot clef transient
    private transient String motDePasse;
    private LocalDate jdn = LocalDate.now();

    private static int cpt; // les variable de classe ne sont pas sérialisée

    public Personne() {
        cpt++;
    }

    public Personne(String prenom, String nom, Adresse adresse) {
        this();
        this.prenom = prenom;
        this.nom = nom;
        this.adresse = adresse;
    }

    public Personne(String prenom, String nom, Adresse adresse, String motDePasse) {
        this();
        this.prenom = prenom;
        this.nom = nom;
        this.adresse = adresse;
        this.motDePasse = motDePasse;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public LocalDate getJdn() {
        return jdn;
    }

    public void setJdn(LocalDate jdn) {
        this.jdn = jdn;
    }
    
    public static int getCpt() {
        return cpt;
    }

    @Override
    public String toString() {
        return "Personne [prenom=" + prenom + ", nom=" + nom + ", adresse=" + adresse + ", motDePasse=" + motDePasse
                + ", jdn=" + jdn + "]";
    }

}
