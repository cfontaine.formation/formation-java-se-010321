package fr.dawan.formation;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

public class TestProperties {

    public static void main(String[] args) {
        // écriture des propriétées

        // Properties représente un ensemble persistant de propriétés (clé=valeur)
        // uniquement des chaines de caractères
        Properties prop = new Properties();
        // On ajoute des propriétés
        prop.setProperty("Version", "1.0");
        prop.setProperty("username", "johndoe");
        prop.setProperty("asupprimer", "aaa");
        // Supression d'une propriété
        prop.remove("asupprimer");

        try {
            // On peut sauver les propriétés :
            // - dans un fichier .properties
            prop.store(new FileWriter("c:\\Formations\\TestIO\\test.properties"), "test d'écriture");
            // - dans un fichier .xml
            prop.storeToXML(new FileOutputStream("c:\\Formations\\TestIO\\test.xml"), "test d'écriture xml", "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }

        prop.clear(); // effacer toutes les propriétés

        // Lecture des propriétées
        Properties propL = new Properties();
        try {
            // propL.load(new FileReader("c:\\Formations\\TestIO\\test.properties"));
            propL.loadFromXML(new FileInputStream("c:\\Formations\\TestIO\\test.xml"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Récupérer la valeur à partir de la clé
        System.out.println(propL.getProperty("Version"));
        System.out.println(propL.getProperty("username"));
        System.out.println(propL.getProperty("nexistepas")); // si la clé n'existe pas => null
        System.out.println(propL.getProperty("nexistepas", "valeur par défaut")); // si la clé n'existe pas => valeur par défaut

        // Récupérer un set contenant toutes les clés
        Set<Object> keys = propL.keySet();
        for (Object k : keys) {
            System.out.println("Clé = " + k);
        }
    }

}
