package fr.dawan.formation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import fr.dawan.formation.beans.Employe;

public class Main {

    public static void main(String[] args) {

        System.out.println(File.separatorChar); // File.séparator Le caractère de séparation par défaut dépendant du système
        
        ecrireFichierTexte("c:\\Formations\\TestIO\\test1.txt");
        ecrireFichierTexteBuff("c:\\Formations\\TestIO\\test2.txt");
        ecrireFichierTextePrint("c:\\Formations\\TestIO\\test3.txt");
        lireFichierTexte("c:\\Formations\\TestIO\\test1.txt");
        lireFichierTexteBuff("c:\\Formations\\TestIO\\test2.txt");

        
        // Parcourir le répertoire du projet 02-poo
        // File : représentation des chemins d'accès aux fichiers et aux répertoires
        parcourir(new File("c:\\Formations\\TestIO\\rep"));
        parcourir(new File("C:\\Formations\\Workspace\\02-poo"));
        
        // Exercice: copie de fichier
        copier("c:\\Formations\\TestIO\\Logo dawan.png", "c:\\Formations\\TestIO\\Logo_copie.png");
        
        try {
            List<Employe> lst=Export.importCsv("c:\\Formations\\TestIO\\test.csv");
            for(Employe e : lst) {
                System.out.println(e);
            }
            Export.exportCsv("c:\\Formations\\TestIO\\export.csv", lst);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Ecrire dans un fichier texte
    public static void ecrireFichierTexte(String chemin) {
        FileWriter fw = null;
        try {
            fw = new FileWriter(chemin);    
            for (int i = 0; i < 10; i++) {
                fw.write("Hello World");    // écriture d'une chaine dans le fichier
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fw != null) {
                try {
                    fw.close();             // fermeture du flux
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // Utilisation de try with ressource pour fermer automtiquement le flux vers le fichier (à partir du java 7)
    // try with ressource fonctionne avec tous les objets implémentant l'interface AutoCloseable
    public static void ecrireFichierTexteBuff(String chemin) {
        // On utilise un BufferedWriter, il n'a pas accès directement au fichier, il faut passer par un FileWriter
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(chemin, true))) {    // true : on ajoute des données au fichier, false : le fichier est écrasé (par défaut)
            for (int i = 0; i < 10; i++) {
                bw.write("Hello World");
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    // Ecrire dans un fichier texte avec PrintWriter
    public static void ecrireFichierTextePrint(String chemin) {
        try (PrintWriter pw = new PrintWriter(new FileWriter(chemin, true))) {
            for (int i = 0; i < 10; i++) {
                pw.println(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Lecture d'un fichier texte
    public static void lireFichierTexte(String chemin) {
        char[] buff = new char[10];
        try (FileReader fr = new FileReader(chemin)) {
            while (fr.read(buff) > 0) { // lecture de 10 caractères maximum dans le fichier
                for (char c : buff) {   // lorsque l'on atteint la fin du fichier read retourne -1 
                    System.out.print(c);

                }
                System.out.println("");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Lecture d'un fichier texte avec BufferedReader
    public static void lireFichierTexteBuff(String chemin) {
        try (BufferedReader br = new BufferedReader(new FileReader(chemin))) {
            String line = null;
            while (true) {
                line = br.readLine();    // lecture d'une ligne dans le fichier
                if (line == null) {     // lorsque l'on atteint la fin du fichier readLine retourne null
                    break;
                }
                System.out.println(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Parcourir un système de fichiers
    public static void parcourir(File f) {
        if (f.exists()) {   // Test l'existence du fichier ou du dossier
            if (f.isDirectory()) {  // si c'est un dossier
                System.out.println("Répertoire= " + f.getName());    // affichage du nom du répertoire
                System.out.println("___________________");
                File[] tf = f.listFiles();  // Récupération du contenu du dossier
                for (File fi : tf) {
                    parcourir(fi);      // Appel récursif sur chaque fichier du dossier
                }
            } else {
                System.out.println(f.getName());    // affichage du nom du fichier
            }
        } else {
            f.mkdir();  // si le chemin n'existe pas on crée un dossier
        }
    }

    
    // Exercice: faire une méthode qui va copier un fichier octet par octet
    public static void copier(String pathSource, String pathTarget) {
        try (FileInputStream fis = new FileInputStream(pathSource); FileOutputStream fos = new FileOutputStream(pathTarget)) {
            while (true) {
                int b = fis.read();
                if (b == -1) {
                   break;
                }
                fos.write(b);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
