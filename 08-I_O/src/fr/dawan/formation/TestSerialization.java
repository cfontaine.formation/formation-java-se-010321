package fr.dawan.formation;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import fr.dawan.formation.beans.Adresse;
import fr.dawan.formation.beans.Personne;

public class TestSerialization {

    public static void main(String[] args) {
        Adresse adr = new Adresse("rue esquermoise", "Lille", "59000");
        Personne p1 = new Personne("John", "Doe", adr,"123456");

        serialisation(p1, "c:\\Formations\\TestIO\\personne.dat");
        Personne p2 = deSerialisation("c:\\Formations\\TestIO\\personne.dat");
        System.out.println(p2);
        
        serialisationXML(p1, "c:\\Formations\\TestIO\\personne.xml");
        Personne p2xml = deSerialisationXML("c:\\Formations\\TestIO\\personne.xml");
        System.out.println(p2xml);
    }

    // Sérialisation
    // l'objet Personne va être sérialiser ainsi que tous les objets qu'il contient
    // ObjectOutputStream permet de persiter un objet ou une grappe d'objet
    public static void serialisation(Personne personne, String chemin) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(chemin))) {
            oos.writeObject(personne);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
  
    // Désérialisation
    // ObjectInputStream permet de désérialiser
    public static Personne deSerialisation(String chemin) {
        Personne p = null;
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(chemin))) {
            Object obj = ois.readObject();
            if (obj instanceof Personne) {
                p = (Personne) obj;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return p;
    }
    
    // Sérialisation XML
    // Pour pouvoir sérializer un objet en xml avec XMLEncoder, il doit avoir un constructeur par défaut
    // transcient ne fonction ne pas avec la sérialisation xml
    public static void serialisationXML(Personne personne, String chemin) {
       // Empêcher la sérialisation d'un attribut en utilisant la reflexion
        BeanInfo info=null;
        try {
            info = Introspector.getBeanInfo(Personne.class);
            PropertyDescriptor[] propertyDescriptors = info.getPropertyDescriptors();
            for (PropertyDescriptor descriptor : propertyDescriptors) {
                if (descriptor.getName().equals("motDePasse")) {
                    descriptor.setValue("transient", Boolean.TRUE);
                }
            }
        } catch (IntrospectionException e1) {
            e1.printStackTrace();
        }
        
        try (XMLEncoder encoder = new XMLEncoder(new FileOutputStream(chemin))) {
            encoder.writeObject(personne);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    
    // Désérialisation XML => XMLDecoder
    public static Personne deSerialisationXML(String chemin) {
        Personne p = null;
        try (XMLDecoder decoder = new XMLDecoder(new FileInputStream(chemin))) {
            Object obj = decoder.readObject();
            if (obj instanceof Personne) {
                p = (Personne) obj;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return p;
    }
}
