package fr.dawan.formation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import fr.dawan.formation.beans.Contrat;
import fr.dawan.formation.beans.Employe;

// Exercice Export/import CSV
public class Export {
    
    public static void exportCsv(String path,List<Employe> employes) throws IOException {
        try(BufferedWriter bw=new BufferedWriter(new FileWriter(path))){
            for(Employe e: employes) {
                bw.write(employeToStr(e));
                bw.newLine(); 
            }
        }
    }

    public static List<Employe> importCsv(String path) throws IOException{
        List<Employe> lstEmploye=new ArrayList<>();
        try(BufferedReader br=new BufferedReader(new FileReader(path))){
           String line=null;
            while(true) {
                line=br.readLine();
                if(line==null) {
                    break;
                }
                Employe tmp=StrToEmploye(line);
                if(tmp!=null) {
                    lstEmploye.add(tmp);
                }
           }
        }
        return lstEmploye;
    }
    
    private static String employeToStr(Employe e) {
        StringBuilder sb=new StringBuilder();
        sb.append(e.getTypeContrat());
        sb.append(";");
        sb.append(e.getPrenom());
        sb.append(";");
        sb.append(e.getNom());
        sb.append(";");
        sb.append(e.getDateNaissance());
        sb.append(";");
        sb.append(e.getSalaire());
        return sb.toString();
    }
    
    private static  Employe StrToEmploye(String line) {
        Employe e=null;
        StringTokenizer tok=new StringTokenizer(line,";");
        if(tok.countTokens()==5) {
            e=new Employe();
            e.setTypeContrat(Contrat.valueOf(tok.nextToken()));
            e.setPrenom(tok.nextToken());
            e.setNom(tok.nextToken());
            e.setDateNaissance(LocalDate.parse(tok.nextToken()));
            e.setSalaire(Double.parseDouble(tok.nextToken()));
        }
        return e;
    }
}
