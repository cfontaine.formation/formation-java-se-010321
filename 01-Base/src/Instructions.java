import java.util.Scanner;

public class Instructions {

    public static void main(String[] args) {
        // Condition if
        Scanner sc = new Scanner(System.in);
        int si = sc.nextInt();
        if (si > 20) {
            System.out.println("la valeur saisie est > 20");
        } else {
            System.out.println("la valeur saisie est < 20");
        }

        // Exercice: Trie de 2 valeurs
        // Saisir 2 nombres et afficher ces nombres triés dans l'ordre croissant sous la forme 1.5 < 10.5
        System.out.print("Entrer un nombre ");
        double d1 = sc.nextDouble();
        System.out.println("Saisir un deuxième entier");
        double d2 = sc.nextDouble();
        if (d1 > d2) {
            System.out.println(d2 + " < " + d1);
        } else {
            System.out.println(d1 + " < " + d2);
        }

        // Exercice: Intervalle
        // Saisir un nombre et dire s'il fait parti de l'intervalle -4 (exclus) et 7 (inclus)
        System.out.println("Entrer un nombre");
        int val = sc.nextInt();
        if (val > -4 && val <= 7) {
            System.out.println(val + " fait parti de l'interval");
        }

        // Condition switch
        int jours = sc.nextInt();
        switch (jours) {
        case 1:
            System.out.println("Lundi");
            break;
        case 6:
        case 7:
            System.out.println("week end !");
            break;
        default:
            System.out.println("autre jour");
            break;
        }

        // Exercice Calculatrice
        // Faire un programme calculatrice
        // Saisir dans la console
        // - un double v1
        // - une chaine de caractère opérateur qui a pour valeur valide : + - * /
        // - un double v2
        // Afficher:
        // - Le résultat de l’opération
        // - Une message d’erreur si l’opérateur est incorrecte
        // - Une message d’erreur si l’on fait une division par 0
        System.out.print("Entrer une opération: nombre opérateur nombre");
        double v1 = sc.nextDouble();
        String op = sc.next();
        double v2 = sc.nextDouble();
        switch (op) {
        case "+":
            System.out.println(v1 + " + " + v2 + " = " + (v1 + v2));
            break;
        case "-":
            System.out.println(v1 + " - " + v2 + " = " + (v1 - v2));
            break;
        case "*":
            System.out.println(v1 + " * " + v2 + " = " + (v1 * v2));
            break;
        case "/":
            if (v2 == 0.0) { // (v2> -0.00001 && v2< 0.00001)
                // v2>-0.000001 && v2<0.000001 après un calcul, il ne vaut pas faire des tests
                // d'égalités mais tester un intervalle pour prendre en compte les erreurs de calculs
                // float , double sont des valeurs approchées. Ici, comme c'est une valeur saisie, on peut tester l'égalité
                System.out.println("Division par 0");
            } else {
                System.out.println(v1 + " / " + v2 + " = " + (v1 / v2));
            }
            break;
        default:
            System.out.println("L'opérateur " + op + " n'est pas valide");
        }

        // Condition opérateur ternaire

        // Exercice parité
        // Créer un programme qui indique, si le nombre entier saisie dans la console
        // est paire ou impaire en utilisant un opérateur ternaire
        int v = sc.nextInt();
        System.out.println(v % 2 == 0 ? "paire" : "impaire");

        // Boucle while / do while
        int k = 0;
        int somme = 0;
        do {
            somme += k;
            k++;
        } while (k <= 10);
        System.out.println("Somme= " + somme);

        // Boucle for
        for (int i = 0; i < 10; i++) {
            System.out.println("i= " + i);
        }

        // Instructions de branchement
        for (int i = 0; i < 10; i = i + 1) {
            System.out.println("i= " + i);
            if (i == 3) {
                break; // break => termine la boucle
            }
        }

        for (int i = 0; i < 10; i = i + 1) {

            if (i == 3) {
                continue; // continue => on passe à l'itération suivante
            }
            System.out.println("i= " + i);
        }

        EXIT_LOOP: for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 5; j++) {
                System.out.println("i= " + i + "j=" + j);
                if (j == 3) {
                    break EXIT_LOOP; // se branche sur le label EXIT_LOOP et quitte les 2 boucles imbriquées
                }
            }
        }
          
        // Exercice: Table de multiplication
        // Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9  
        //
        // 1 X 4 = 4
        // 2 X 4 = 8
        //  …
        // 9 x 4 = 36
        //
        // Si le nombre saisie est en dehors de l’intervalle 1 à 9 on arrête sinon on redemande une nouvelle valeur
        
        System.out.println("Entrer un nombre");
        for(;;) {   // while(true)
            int m=sc.nextInt();
            if(m<1 || m>9) {
                break;
            }
            for(int i=1;i<10;i++) {
                System.out.println(i + " x " + m + " = " + (m*i));
            }
        }
        
        
        // Exercice: Quadrillage 
        // un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne  

        // ex: pour 2 3  
        // [ ][ ]  
        // [ ][ ]  
        // [ ][ ]  
        
        System.out.println("Entrer le nombre de colonne");
        int col=sc.nextInt();
        System.out.println("Entrer le nombre de ligne");
        int row=sc.nextInt();
        
        for(int r=0;r<row;r++) {
            for(int c=0;c<col;c++) {
                System.out.print("[ ] ");
            }
            System.out.println("");
        }
        
        sc.close();
    }

}
