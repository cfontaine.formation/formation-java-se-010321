import java.util.Scanner;

public class Tableaux {

    public static void main(String[] args) {

        double[] tab1 = new double[3]; // Tableau à une dimension Déclaration
        // par défaut les éléments du tableau sont initialisés:
        // - entier ->  0
        // - réel   -> 0.0
        // - char   -> '\u0000'
        // - boolean -> false
        // - type référence -> null

        // Parcourir un tableau
        for (int i = 0; i < 3; i++) {
            System.out.println(tab1[i]);
        }

        // Initialisation des éléments du tableau
        tab1[0] = 10.0;
        tab1[1] = 3.0;
        tab1[2] = 10.5;

        System.out.println(tab1[2]); // affichage du troisième élément du tableau

        // Parcourir un tableau complétement "foreach"
        for (int i = 0; i < tab1.length; i++) {
            System.out.println(tab1[i]);
        }

        System.out.println("nb élément =" + tab1.length); // affichage de la taille du tableau

        // Tableau à une dimension Déclaration et initialisation
        String[] tabStr = { "Hello", "Bonjour", "azerty", "world" };

        for (String str : tabStr) {
            System.out.println(str);
        }

        // Date [] tab2= {new Date(),new Date(), new Date()};

        // Exercice Tableau
        // 1) Trouver la valeur maximale et la moyenne d’un tableau de 5 entier: -7,4,8,0,-3
        // 2) Modifier le programme pour faire la saisie de la taille du tableau et saisir
        //    les éléments du tableau

        // int t[] = {-7,4,8,0,-3}; // 1

        Scanner sc = new Scanner(System.in); // 2
        System.out.println("Entrer la taille du tableau");
        int size = sc.nextInt();
        int[] t = new int[size];

        for (int i = 0; i < t.length; i++) {
            System.out.print("t[" + i + "]=");
            t[i] = sc.nextInt();
        }

        int max = t[0]; // Integer.MIN_VALUE;
        double somme = 0.0;
        for (int v : t) {
            if (v > max) {
                max = v;
            }
            somme += v;
        }
        System.out.println("Maximum = " + max);
        System.out.println("Moyenne = " + somme / t.length);
        sc.close();

        // Tableau à 2 dimensions symétrique
        char[][] tabChr = new char[2][4];
        tabChr[1][3] = 'a';

        // Parcourir un tableau à 2 dimensions "foreach"
        for (char tl[] : tabChr) {
            for (char c : tl) {
                System.out.print(c + " | ");
            }
            System.out.println(" ");
        }

        // Taille du tableau
        System.out.println(tabChr.length); // Nombre d'élément de la 1ère dimension: 2
        System.out.println(tabChr[0].length); // Nombre d'élément de la 2ème dimension: 4

        int[][] tab2D = { { 1, 3 }, { 4, 6 }, { 1, 9 } };
        for (int i = 0; i < tab2D.length; i++) {
            for (int j = 0; j < tab2D[i].length; j++) {
                System.out.print(tab2D[i][j] + " | ");
            }
            System.out.println(" ");
        }

        // Tableau à 2 dimensions asymétrique
        int[][] tab2d2 = new int[3][];
        tab2d2[0] = new int[4];
        tab2d2[1] = new int[2];
        tab2d2[2] = new int[3];

        for (int ti[] : tab2d2) {
            for (int vi : ti) {
                System.out.print(vi + " | ");
            }
            System.out.println(" ");
        }

        char[][] tabChr2 = { { 'a' }, { 'z', 'e', 'z' }, { 'r', 't' } };
        for (int i = 0; i < tabChr2.length; i++) {
            for (int j = 0; j < tabChr2[i].length; j++) {
                System.out.print(tab2D[i][j] + " | ");
            }
            System.out.println(" ");
        }

    }

}
