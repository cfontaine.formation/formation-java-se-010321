import java.util.Scanner;

public class Methodes {

    public static void main(String[] args) {
        // Appel de méthode
      /*  System.out.println(multiplication(3.4, 5.5));
        System.out.println(multiplication(1, 5));
        afficher("Azerty");

        // Exercice: appel de la méthode maximum
        Scanner sc = new Scanner(System.in);
        int i = sc.nextInt();
        int j = sc.nextInt();
        System.out.println("Maximum=" + maximum(i, j));

        // test des arguments par valeurs (paramètre de type primitif)
        int v = 12;
        testParamVal(v);
        System.out.println(v);

        // test des arguments par valeurs (paramètre de type référence)
        StringBuilder sb = new StringBuilder("Value");
        testParamRef(sb);
        System.out.println(sb);

        // test des arguments par valeurs (Modification de l'état de l'objet)
        StringBuilder sb2 = new StringBuilder("Value");
        testParamRefObj(sb2);
        System.out.println(sb2);

        // Nombre d'arguments variable
        System.out.println(moyenne());
        System.out.println(moyenne(1.0, 6, 5, 3.0));
        System.out.println(moyenne(1.0, 6, 5, 3.0, 7.9, 5.6));

        // Surcharge de méthode
        System.out.println(somme(1, 2));
        System.out.println(somme(1.6, 2.4));
        System.out.println(somme(1.0, 2.5, 3.5));
        System.out.println(somme("1", "2"));
        System.out.println(somme(1.0, 2.5, 3));
        byte b1 = 2;
        byte b2 = 3;
        System.out.println(somme(b1, b2));

        // Méthode Récursive
        System.out.println(factorial(3));

        // Affichage des paramètres passés à la méthode main
        for (String a : args) {
            System.out.println(a);
        }*/
        Scanner sc = new Scanner(System.in);
        int[] tab=saisirTab(sc);
        affTab(tab);
        System.out.println("Maximum="+ maxTab(tab));
        

        sc.close();
    }

    // Déclaration d'une méthode
    static double multiplication(double v1, double v2) {
        double tmp = v1 * v2;
        return tmp; // L'instruction return
                    // - Interrompt l'exécution de la méthode
                    // - Retourne la valeur (expression à droite)
    }

    static void afficher(String str) {   // Méthode qui ne retourne rien => void
        System.out.println(str);
        //   avec void => return; ou return peut être omis 
    }

    // Exercice Maximum
    // Écrire une fonction maximum qui prends en paramètre 2 nombres et retourne
    // le maximum
    // Saisir 2 nombres et afficher le maximum entre ces 2 nombres
    static int maximum(int a, int b) {
        if (a > b) {
            return a;
        } else {
            return b;
        }
        // ou return a>b?a:b;
    }

    // test des arguments par valeurs (paramètre de type primitif)
    static void testParamVal(int a) {
        System.out.println(a);
        a = 10; // la modification du paramètre a n'a pas de répercution en dehors de la méthode
        System.out.println(a);
    }

    // test des arguments par valeurs (paramètre de type référence)
    static void testParamRef(StringBuilder sb) {
        System.out.println(sb);
        sb = new StringBuilder("Other Value"); // si on modifie la valeur de la référence str, il n'a pas répercution en dehors de la méthode
        System.out.println(sb);
    }

    // test des arguments par valeurs (Modification de l'état de l'objet)
    static void testParamRefObj(StringBuilder sb) {
        System.out.println(sb);
        sb.append(" modif");   // mais on peut modifier l'état de l'objet (la référence n'est pas modifiée)
        System.out.println(sb);
    }

    // nombre d'arguments variable
    static double moyenne(double... val) {
        if (val.length == 0) {
            return 0.0;
        }
        double s = 0.0;
        for (double v : val) {
            s += v;
        }
        return s / val.length;
    }

    // Surcharge de méthode
    // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même nom, mais leurs signatures doient être différentes
    // La signature d'une méthode correspond aux types et nombre de paramètres
    // Le type de retour ne fait pas partie de la signature
    static int somme(int a, int b) {
        System.out.println("2 entiers");
        return a + b;
    }

    static double somme(double a, double b) {

        System.out.println("2 doubles");
        return a + b;
    }

    static double somme(double a1, int b1) {
        System.out.println("1double et 1 entier");
        return a1 + b1;
    }

    static double somme(double a, double b, double c) {
        System.out.println("3 doubles");
        return a + b + c;
    }

    static String somme(String s1, String s2) {
        System.out.println("2 chaine de caractères");
        return s1 + s2;
    }

    // Méthode récursive
    static int factorial(int n) { // factoriel= 1* 2* … n
        if (n <= 1) { // condition de sortie
            return 1;
        } else {
            return factorial(n - 1) * n;
        }
    }
    
    static void affTab(int[] tab) {
        System.out.print("[ ");
        for(int v :tab) {
            System.out.print(v +" ");
        }
        System.out.println("]");
    }
    
    static int [] saisirTab(Scanner scan) {
        System.out.println("Saisir la taille du tableau");
        int size=scan.nextInt();
        
        int[] tmp=new int[size];
        
        for(int i=0;i<tmp.length;i++) {
            System.out.print("tab["+i+"]=");
            tmp[i]=scan.nextInt();
        }
        return tmp;
    }
    
    static int maxTab(int[] tab) {
        int max = tab[0]; 
        for (int v : tab) {
            if (v > max) {
                max = v;
            }
        }
        return max;
    }

}
