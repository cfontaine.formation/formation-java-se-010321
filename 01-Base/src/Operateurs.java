import java.util.Scanner;

public class Operateurs {

    public static void main(String[] args) {
        // Opérateur arthméthique
        int a = 123;
        int b = 34;
        int c = a + b;
        int m = a % 2; // % modulo => reste de la division entière
        System.out.println("res=" + c + " modulo=" + m);

        // Pré-incrémentation
        int inc = 0;
        int res = ++inc; // Incrémentation de inc et affectation de res avec la valeur de inc
        System.out.println(inc + " " + res); // inc=1 et res=1

        // Post-incrémentation
        inc = 0;
        res = inc++; // Affectation de res avec la valeur de inc et incréméntation de inc
        System.out.println(inc + " " + res); // inc=1 et res=0

        // Affectation composé
        a += 5; // correspont à a=a+5;
        System.out.println(a);

        // Opérateur de comparaison
        // Une comparaison a pour résultat un booléen
        boolean tst = a == 3; // false
        System.out.println(tst);
        tst = a == 128; // true
        System.out.println(tst);

        // Opérateur logique
        System.out.println(!tst); // ! => Opérateur non
        tst = a == 128 && b > 40;
        System.out.println(tst);

        // Opérateur court-circuit && et ||
        // && dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
        tst = a == 12 && (b < 40 || b == 1);
        System.out.println(tst);
        // || dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
        tst = a == 128 || b < 40;
        System.out.println(tst);

        // Opérateur Binaires (bit à bit)
        int bin = 0b10101;
        System.out.println(~bin); // complément
        System.out.println(bin & 0b111); // et => 101 5
        System.out.println(bin | 0b111); // ou => 101111 23
        System.out.println(bin ^ 0b111); // ou exclusive => 101010

        // Opérateur de décalage
        System.out.println(0b111 >> 2); // décallage à droite de 2 bits => 1, Insertion à droite bit de signe
        System.out.println(0b111 >>> 2); // décallage à droite de 2 bits => 1, Insertion à droite 0
        System.out.println(0b111 << 3); // décallage à gauche de 3 bits => 111000

        // Promotion numérique
        // 1=> Le type le + petit est promu vers le + grand type des deux
        float prf = 23.5f;
        double prd = 34.2;
        double r1 = prf + prd; // 4=> Après une promotion le résultat aura le même type
        System.out.println(r1);

        // 2=> La valeur entière est promue en virgule flottante
        int pri = 10;
        double r2 = pri + prd;
        System.out.println(r2);

        // 3=> byte, short, char sont promus en int
        byte b1 = 12;
        byte b2 = 56;
        int r3 = b1 + b2;
        System.out.println(r3);

        // Saisie dans la console => Scanner
        Scanner scan = new Scanner(System.in);
        int i = scan.nextInt();
        String msg = scan.next();
        System.out.println(i + " " + msg);

        // Exercice Salutation
        // Faire un programme qui:
        // - Affiche le message: Entrer votre nom
        // - Permet de saisir le nom
        // - Affiche Bonjour, complété du nom saisie
        System.out.println("Entrer votre prénom");
        String prenom = scan.next();
        System.out.println("Bonjour, " + prenom);

        // Exercice Somme
        // Saisir 2 chiffres et afficher le résultat dans la console
        // sous la forme 1 + 3 = 4
        System.out.print("Entrer un nombre ");
        int v1 = scan.nextInt();
        System.out.print("Entrer un deuxième nombre ");
        int v2 = scan.nextInt();
        int add = v1 + v2;
        System.out.println(v1 + " + " + v2 + " = " + add);

        scan.close();
    }

}
