
public class DataType {
    
    // Méthode main point d'entré du programme
    public static void main(String[] args) {

        // Déclaration d'une variable
        int i;
        // System.out.println(i); // En java, on ne peut pas utiliser une variable non
        // initiaisée

        // Initialisation d'une variable
        i = 42;
        System.out.println(i);

        // Déclaration multiple
        double hauteur, largeur;
        hauteur = 1.0;
        largeur = 2.0;
        System.out.println(hauteur + " " + largeur);
        
        // Déclaration et initialisation d'une variable
        double d = 2.5;
        System.out.println(i + " d=" + d);

        // Déclaration et initialisation multiple
        boolean test = true, isOk = true;
        System.out.println(test + " " + isOk);

        
        
        // Par défaut une valeur littérale entière est de type int
        long l = 1234578478499880L; // L -> littéral de type long

        // Par défaut une valeur littérale réel est de type double
        float f = 2.5f; // f -> littéral de type float
        System.out.println(l + "  " + f);

        // Littéraux changement de base
        int dec = 103; // décimal (base 10) par défaut
        int hex = 0xf3a0; // 0x -> hexadécimal
        int oct = 045; // 0 -> Octal
        int bin = 0b0101010; // 0b -> binaire
        System.out.println(dec + " " + hex + " " + oct + " " + bin);

        // Séparateur _
        // Pas de séparateur _ en début, en fin , avant et après la virgule
        int mille = 1_000;
        double sep = 1_000_230.50;
        System.out.println(mille + " " + sep);

        // Littéral caractère
        char ch = 'a';
        char chUtf8 = '\u0061'; // en UTF-8
        System.out.println(ch + "  " + chUtf8);

        // Littéral réel
        double d1 = 10.56;
        double d2 = 1.23e3;
        System.out.println(d1 + " " + d2);

        // Littéral boolean
        boolean b = true; // false
        System.out.println(b);

        
        
        // Transtypage implicite (pas de perte de donnée)
        // Type inférieur vers un type supérieur
        short s = 23;
        int tii = s;
        // Entier vers un réel
        double tid = tii;
        System.out.println(s + " " + tii + " " + tid);

        // Transtypage explicite : cast = (nouveauType)
        double ted = 234.45;
        // Entier vers un réel
        int tei = (int) ted;
        System.out.println(tei + " " + ted);
        // Type supérieur vers un type inférieur
        short sei = (short) tei;
        System.out.println(sei);     

        // Transtypage explicite, dépassement de capacité
        int to = 300;           // 00000001 00101100 300
        byte tb = (byte) to;    //          00101100 44
        System.out.println(to + " " + tb);

        
        
        // Type référence
        String str1 = new String("Hello"); // ou "Hello";
        String str2 = null; // str2 ne référence aucun objet
        System.out.println(str1 + " " + str2);
        str2 = str1; // str1 et str2 font références au même objet
        System.out.println(str1 + " " + str2);
        str1 = null;
        System.out.println(str1 + " " + str2);
        str2 = null;
        // str1 et str2 sont égales à null
        // Il n'y a plus de référence sur l'objet, il éligible à la destruction par le
        // garbage collector
        System.out.println(str1 + " " + str2);
    }

}
