package fr.dawan.formation;

import java.time.LocalDate;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import fr.dawan.formation.beans.Employe;

public class EmployeTableModel extends AbstractTableModel {


    private static final long serialVersionUID = 1L;

    private List<Employe> employes;
    
    public EmployeTableModel(List<Employe> employes) {
        super();
        this.employes = employes;
    }

    @Override
    public int getRowCount() {

        return employes.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Employe dataRow=employes.get(rowIndex);
        switch(columnIndex) {
        case 0:
            return dataRow.getTypeContrat();
        case 1:
            return dataRow.getPrenom();
        case 2:
            return dataRow.getNom();
        case 3:
            return dataRow.getDateNaissance();
        case 4:
            return dataRow.getSalaire();
        }
        return null;
    }

    @Override
    public String getColumnName(int column) {
        switch(column) {
        case 0:
            return "Contrat";
        case 1:
            return "Prénom";
        case 2:
            return "Nom";
        case 3:
            return "Date de Naissance";
        case 4:
            return "Salaire";
        }
        return null;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch(columnIndex) {
        case 0:
        case 1:
        case 2:
            return String.class;
        case 3:
            return LocalDate.class;
        default:
            return Double.class;
        }
    }
    

}
