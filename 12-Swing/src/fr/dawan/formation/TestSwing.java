package fr.dawan.formation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class TestSwing {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Test Swing");
        frame.setSize(800, 600);
        frame.setMinimumSize(new Dimension(300, 200));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        BoutonAction ba=new BoutonAction();
        // frame.setLayout(null); // Positionnement absolue
        // frame.getContentPane().setLayout(new FlowLayout(FlowLayout.LEFT,10,30));
        // frame.getContentPane().setLayout(new BorderLayout(10,20));
        JButton bp1 = new JButton("ok");
        bp1.setPreferredSize(new Dimension(200, 100));
        bp1.addActionListener(ba);
        bp1.setActionCommand("bp1");
        
        //        frame.getContentPane().add(bp1,BorderLayout.NORTH);
//        
//        bp1.addActionListener(new ActionListener() {
//            
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                System.out.println("BP1");
//                
//            }
//        });
//        
//        bp1.addMouseListener(new MouseAdapter() {
//
//            @Override
//            public void mouseEntered(MouseEvent e) {
//                System.out.println("Enter" + e.getX());
//            }
//
//
//
//            @Override
//            public void mouseMoved(MouseEvent e) {
//                System.out.println(e.getX() + " " + e.getY());
//            }
//
//     
//        });
        JButton bp2 = new JButton("not ok");
        bp2.setPreferredSize(new Dimension(200, 100));
        bp2.addActionListener(ba);
        bp2.setActionCommand("bp2");
        //        frame.getContentPane().add(bp2,BorderLayout.CENTER);

        // GridLayout gl=new GridLayout(3,3,20,20);
//        gl.setRows(3);
//        gl.setColumns(3);
//        frame.getContentPane().setLayout(gl);
//        for(int i= 0;i<9;i++) {
//            frame.getContentPane().add(new JButton(Integer.toString(i)));
//        }
        JPanel pan1 = new JPanel();
        pan1.setLayout(new FlowLayout());
        pan1.add(bp1);
        pan1.add(bp2);
        frame.getContentPane().setLayout(new BorderLayout());
        frame.getContentPane().add(pan1, BorderLayout.SOUTH);
        frame.setVisible(true);

    }

}
