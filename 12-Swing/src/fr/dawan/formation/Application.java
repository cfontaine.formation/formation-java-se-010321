package fr.dawan.formation;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;

import fr.dawan.formation.dao.EmployeDAO2;

public class Application {

    private JFrame frame;
    private JTable table;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Application window = new Application();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public Application() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 554, 370);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JPanel panel = new JPanel();
        frame.getContentPane().add(panel, BorderLayout.SOUTH);
        
        JButton btnNewButton_2 = new JButton("Info");
        btnNewButton_2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(frame, "Message d'information","Information",JOptionPane.INFORMATION_MESSAGE);
            }
        });
        panel.add(btnNewButton_2);
        
        JButton btnNewButton = new JButton("Ajouter");
        panel.add(btnNewButton);
        
        JButton btnNewButton_1 = new JButton("Quitter");
        panel.add(btnNewButton_1);
        
        table = new JTable();

        EmployeDAO2 dao=new EmployeDAO2();
        table.setModel(new EmployeTableModel(dao.findAll(true)));
        frame.getContentPane().add(table, BorderLayout.CENTER);
    }

}
