package fr.dawan.formation;

public class Main {

    public static void main(String[] args) {
       Box<String> strBox=new Box<>();
       strBox.setT("Hello");
       System.out.println(strBox);
       
       Box<Double> strDouble=new Box<>(123.65);
       System.out.println(strDouble);
       System.out.println(Box.equals("azerty", "a"));
       System.out.println(Box.equals(42,30+12));
    }

}
