package fr.dawan.formation;
public class Box <T>{
    private T t;

    
    public Box() {
    }

    public Box(T t) {
        this.t = t;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    @Override
    public String toString() {
        return "Box [t=" + t + "]";
    }
    
    public static <U> boolean equals(U a, U b) {
       return a.equals(b);
    }
    
}
