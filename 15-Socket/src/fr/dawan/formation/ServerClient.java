package fr.dawan.formation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Vector;

public class ServerClient extends Thread {

    private static Vector<ServerClient> clientThreads = new Vector<>();

    private Socket s;

    public ServerClient(Socket socket) {
        s = socket;
        clientThreads.add(this);
    }

    @Override
    public void run() {
        BufferedReader reader = null;
        PrintWriter writer = null;
        try {
            String message = "";
            reader = new BufferedReader(new InputStreamReader(s.getInputStream()));
            writer = new PrintWriter(s.getOutputStream(), true);
            while (!message.equals("quit")) {
                // lecture du flux
                message = reader.readLine();
                // renvoie de l'écho du message
                writer.println("écho: " + message);
                System.out.println("message=" + message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (writer != null) {
                writer.close();
            }
            try {
                s.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        clientThreads.remove(this);
    }

    public static Vector<ServerClient> getClientSocket() {
        return clientThreads;
    }
}
