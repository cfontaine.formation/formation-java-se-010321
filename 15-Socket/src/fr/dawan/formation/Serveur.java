package fr.dawan.formation;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Serveur {

    void launch() {
        ServerSocket servSocket = null;
        try {
            servSocket = new ServerSocket(15001); // Le server socket écoute le port 15001 et attend qu'un client (socket) se connecte
            boolean loop = true;
            while (loop) {
                // Le server socket écoute le port 15001 et attend qu'un client (socket) se connecte
                Socket s = servSocket.accept(); // Lorsqu'un client se connecte la méthode accept retourne un socket
                ServerClient sct = new ServerClient(s); // On crée un thread qui va gérer la connection avec le client
                sct.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                servSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Serveur server = new Serveur();
        server.launch();

    }

}