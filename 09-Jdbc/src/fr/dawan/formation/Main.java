package fr.dawan.formation;

import java.time.LocalDate;
import java.util.List;

import fr.dawan.formation.beans.Employe;
import fr.dawan.formation.dao.EmployeDAO2;
import fr.dawan.formation.enums.Contrat;

public class Main {

    public static void main(String[] args) {
        EmployeDAO2 dao=new EmployeDAO2();
        Employe e=new Employe("Etienne","Marcel",LocalDate.of(1974, 4, 4),2450.0,Contrat.CDI);
        System.out.println(e);
        dao.saveOrUpdate(e, false);
        System.out.println("Insert=" +e);
        e.setTypeContrat(Contrat.CDD);
        dao.saveOrUpdate(e, false);
        System.out.println("UPDATE=" +e);
        System.out.println("FIND BY ID" + dao.findById(1, false));
        dao.remove(e, false);
        List<Employe> lst=dao.findAll(true);
        for(Employe emp: lst) {
            System.out.println(emp);
        }
    }

}
