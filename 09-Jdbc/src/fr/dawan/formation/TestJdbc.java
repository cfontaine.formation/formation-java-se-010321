package fr.dawan.formation;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

import fr.dawan.formation.beans.Employe;
import fr.dawan.formation.enums.Contrat;

public class TestJdbc {

    public static void main(String[] args) {
        Employe e = new Employe("Raoul", "Dupond", LocalDate.of(1965, 9, 1), 3500.0, Contrat.CDI);
        System.out.println(e);
        insertEmploye(e);
        System.out.println(e);
        readEmploye();
    }

    public static void insertEmploye(Employe e) {
        Connection cnx = null;
        try {
            // Chargement du driver de la base de donnée
            Class.forName("org.mariadb.jdbc.Driver");
            // Création de la connection
            cnx = DriverManager.getConnection("jdbc:mariadb://localhost:3306/formation", "root", "dawan");

            cnx.setAutoCommit(false); // Transaction -> désactivation de l'autocommit

            // Création de la requète => Statement / PreparedStatement
            // Statement stm = cnx.createStatement();
//            String req = "INSERT INTO employes(type_contrat,prenom,nom,date_naissance,salaire) VALUE('"
//                    + e.getTypeContrat() + "' , '" + e.getPrenom() + "' , '" + e.getNom() + "','" + e.getDateNaissance() + "',"
//                    + e.getSalaire() + ")";
//            System.out.println(stm.executeUpdate(req)); // execution de la requète
            PreparedStatement ps = cnx.prepareStatement(
                    "INSERT INTO employes(type_contrat,prenom,nom,date_naissance,salaire) VALUE(?,?,?,?,?)",
                    Statement.RETURN_GENERATED_KEYS); // Statement.RETURN_GENERATED_KEYS => permet de récupérer après
                                                      // l'execution de la requète la valeur de la clé primaire générer par la base de donnée 
            ps.setString(1, e.getTypeContrat().toString());
            ps.setString(2, e.getPrenom());
            ps.setString(3, e.getNom());
            ps.setDate(4, Date.valueOf(e.getDateNaissance()));
            ps.setDouble(5, e.getSalaire());
            System.out.println(ps.executeUpdate()); // execution de la requète
            cnx.commit(); // Transaction -> valider les requêtes
            ResultSet rs = ps.getGeneratedKeys(); // getGeneratedKeys récupération de la clé primaire générer par la base de donnée 
            if (rs.next()) {
                e.setId(rs.getLong(1));
            }

        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        } catch (SQLException ex) {
            if (cnx != null) {
                try {
                    cnx.rollback(); // Transaction -> annuler les requêtes
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            ex.printStackTrace();
        } finally {
            if (cnx != null) {
                try {
                    cnx.close(); // Fermer la connection
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }

    }

    public static void readEmploye() {
        Connection cnx = null;
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            cnx = DriverManager.getConnection("jdbc:mariadb://localhost:3306/formation", "root", "dawan");
            Statement stm = cnx.createStatement();
            // ResultSet contient le resultat de la requète SELECT
            ResultSet rs = stm.executeQuery("SELECT id,type_contrat,prenom,nom,date_naissance,salaire FROM employes");
            while (rs.next()) { // next retourne vrai tant qu'il reste des lignes dans l'objet ResultSet
                System.out.println(rs.getLong("id"));
                System.out.println(rs.getString("type_contrat"));
                System.out.println(rs.getString("prenom"));
                System.out.println(rs.getString("nom"));
                System.out.println(rs.getDate("date_naissance"));
                System.out.println(rs.getDouble("salaire"));
            }
            rs.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (cnx != null) {
                try {
                    cnx.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
