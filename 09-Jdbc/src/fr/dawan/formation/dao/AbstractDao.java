package fr.dawan.formation.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import fr.dawan.formation.beans.DbObject;
import fr.dawan.formation.beans.Employe;

public abstract class AbstractDao<T extends DbObject> {

    private static Properties cnxProperties;

    private static Connection cnx;

    public void saveOrUpdate(T entity, boolean close) {
        Connection c = getConnection();
        try {
            if (entity.getId() == 0) {
                insert(entity, c);
            } else {
                update(entity, c);
            }
        } catch (SQLException e) {

            e.printStackTrace();
            // relance exception
        } finally {
            if (close) {
                closeConnection();
            }
        }
    }

    public void remove(Employe entity, boolean close) {
        remove(entity.getId(), close);
    }

    public void remove(long id, boolean close) {
        Connection c = getConnection();
        try {
            remove(id, c);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (close) {
                closeConnection();
            }
        }
    }

    public T findById(long id, boolean close) {
        T e = null;
        Connection c = getConnection();
        try {
            e = findById(id, c);
        } catch (SQLException e1) {
            e1.printStackTrace();
        } finally {
            if (close) {
                closeConnection();
            }
        }
        return e;
    }

    public List<T> findAll(boolean close) {
        List<T> lst = new ArrayList<>();
        Connection c = getConnection();
        try {
            lst = findAll(c);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (close) {
                closeConnection();
            }
        }
        return lst;
    }

    protected abstract void insert(T entity, Connection cnx) throws SQLException;

    protected abstract void update(T entity, Connection cnx) throws SQLException;

    protected abstract void remove(long id, Connection cnx) throws SQLException;

    protected abstract T findById(long id, Connection cnx) throws SQLException;

    protected abstract List<T> findAll(Connection cnx) throws SQLException;

    protected static Connection getConnection() {
        if (cnx == null) {
            if (cnxProperties == null) {
                try (FileInputStream fis = new FileInputStream("c:\\Formations\\TestIO\\db.properties")) {
                    cnxProperties = new Properties();
                    cnxProperties.load(fis);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                Class.forName(cnxProperties.getProperty("Driver"));
                cnx = DriverManager.getConnection(cnxProperties.getProperty("Url"), cnxProperties.getProperty("UserDb"),
                        cnxProperties.getProperty("PasswordDb"));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return cnx;
    }

    protected static void closeConnection() {
        if (cnx != null) {
            try {
                cnx.close();
                cnx = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
