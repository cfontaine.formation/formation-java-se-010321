package fr.dawan.formation.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import fr.dawan.formation.beans.Employe;
import fr.dawan.formation.enums.Contrat;

public class EmployeDAO2 extends AbstractDao<Employe> {

    @Override
    protected void insert(Employe entity, Connection cnx) throws SQLException {
        PreparedStatement ps = cnx.prepareStatement(
                "INSERT INTO employes(type_contrat,prenom,nom,date_naissance,salaire) VALUE(?,?,?,?,?)",
                Statement.RETURN_GENERATED_KEYS);
        ps.setString(1, entity.getTypeContrat().toString());
        ps.setString(2, entity.getPrenom());
        ps.setString(3, entity.getNom());
        ps.setDate(4, Date.valueOf(entity.getDateNaissance()));
        ps.setDouble(5, entity.getSalaire());
        ps.executeUpdate();
        ResultSet rs = ps.getGeneratedKeys();
        if (rs.next()) {
            entity.setId(rs.getLong(1));
        }
    }

    @Override
    protected void update(Employe entity, Connection cnx) throws SQLException {
        PreparedStatement ps = cnx.prepareStatement("UPDATE employes SET  type_contrat=?,prenom=?,nom=?,date_naissance=?,salaire=? WHERE id=?");
        ps.setString(1, entity.getTypeContrat().toString());
        ps.setString(2, entity.getPrenom());
        ps.setString(3, entity.getNom());
        ps.setDate(4, Date.valueOf(entity.getDateNaissance()));
        ps.setDouble(5, entity.getSalaire());
        ps.setLong(6, entity.getId());
        ps.executeUpdate();

    }

    @Override
    protected void remove(long id, Connection cnx) throws SQLException {
        PreparedStatement ps = cnx.prepareStatement("DELETE FROM employes WHERE id=?");
        ps.setLong(1, id);
        ps.executeUpdate();
    }

    @Override
    protected Employe findById(long id, Connection cnx) throws SQLException {
        Employe e = null;
        PreparedStatement ps = cnx.prepareStatement("SELECT type_contrat,prenom,nom,date_naissance,salaire FROM employes WHERE id=?");
        ps.setLong(1, id);
        ResultSet rs = ps.executeQuery();
        if (rs.next()) {
            e = new Employe(rs.getString("prenom"), rs.getString("nom"), rs.getDate("date_naissance").toLocalDate(),
                    rs.getDouble("salaire"), Contrat.valueOf(rs.getString("type_contrat")));
            e.setId(id);
        }
        return e;
    }

    @Override
    protected List<Employe> findAll(Connection cnx) throws SQLException {

        List<Employe> lst = new ArrayList<>();
        Statement stm = cnx.createStatement();
        ResultSet rs = stm.executeQuery("SELECT id,type_contrat,prenom,nom,date_naissance,salaire FROM employes");
        while (rs.next()) {
            Employe tmp = new Employe(rs.getString("prenom"), rs.getString("nom"),
                    rs.getDate("date_naissance").toLocalDate(), rs.getDouble("salaire"),
                    Contrat.valueOf(rs.getString("type_contrat")));
            tmp.setId(rs.getLong("id"));
            lst.add(tmp);
        }
        return lst;
    }

}
