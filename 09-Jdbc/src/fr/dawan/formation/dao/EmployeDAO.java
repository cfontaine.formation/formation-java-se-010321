package fr.dawan.formation.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import fr.dawan.formation.beans.Employe;
import fr.dawan.formation.enums.Contrat;

public class EmployeDAO {
    
   private static Properties cnxProperties;

   private static Connection cnx;
   
    public void saveOrUpdate(Employe entity, boolean close) {
        if(entity.getId()==0) {
            insert(entity,close);
        }else {
            update(entity,close);
        }
    }


    public void remove(Employe entity, boolean close) {
        remove(entity.getId(), close);

    }

    public void remove(long id, boolean close) {
        Connection c=getConnection();
        PreparedStatement ps;
        try {
            ps = c.prepareStatement("DELETE FROM employes WHERE id=?");
            ps.setLong(1, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(close) {
                closeConnection();
            }
        }
    }

    public Employe findById(long id, boolean close) {
        Employe e = null;
        Connection c=getConnection();
        try {
            PreparedStatement ps=c.prepareStatement("SELECT type_contrat,prenom,nom,date_naissance,salaire FROM employes WHERE id=?");
            ps.setLong(1, id);
            ResultSet rs=ps.executeQuery();
            if(rs.next()) {
                e=new Employe(rs.getString("prenom"),rs.getString("nom"),rs.getDate("date_naissance").toLocalDate(),rs.getDouble("salaire"),Contrat.valueOf(rs.getString("type_contrat"))); 
                e.setId(id);
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }finally {
            if(close) {
                closeConnection();
            }
        }
        return e;
    }

    public List<Employe> findAll(boolean close) {
        List<Employe> lst = new ArrayList<>();
        Connection c=getConnection();
        Statement stm;
        try {
            stm = c.createStatement();
            ResultSet rs=stm.executeQuery("SELECT id,type_contrat,prenom,nom,date_naissance,salaire FROM employes");
            while(rs.next()) {
                Employe tmp=new Employe(rs.getString("prenom"),rs.getString("nom"),rs.getDate("date_naissance").toLocalDate(),rs.getDouble("salaire"),Contrat.valueOf(rs.getString("type_contrat"))); 
                tmp.setId(rs.getLong("id"));
                lst.add(tmp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(close) {
                closeConnection();
            }
        }
        return lst;
    }

    private void insert(Employe entity,boolean close) {
        Connection c=getConnection();
        PreparedStatement ps;
        try {
            ps = c.prepareStatement("INSERT INTO employes(type_contrat,prenom,nom,date_naissance,salaire) VALUE(?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, entity.getTypeContrat().toString());
            ps.setString(2, entity.getPrenom());
            ps.setString(3, entity.getNom());
            ps.setDate(4, Date.valueOf(entity.getDateNaissance()));
            ps.setDouble(5, entity.getSalaire());
            ps.executeUpdate();
            ResultSet rs=ps.getGeneratedKeys();
            if(rs.next()) {
                entity.setId(rs.getLong(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(close) {
                closeConnection();
            }
        }
    }
    
    private void update(Employe entity,boolean close) {
        Connection c=getConnection();
        PreparedStatement ps;
        try {
            ps = c.prepareStatement("UPDATE employes SET  type_contrat=?,prenom=?,nom=?,date_naissance=?,salaire=? WHERE id=?");
            ps.setString(1, entity.getTypeContrat().toString());
            ps.setString(2, entity.getPrenom());
            ps.setString(3, entity.getNom());
            ps.setDate(4, Date.valueOf(entity.getDateNaissance()));
            ps.setDouble(5, entity.getSalaire());
            ps.setLong(6, entity.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(close) {
                closeConnection();
            }
        }
        
    }
    
    private static Connection getConnection() {
       if(cnx==null) {
        if(cnxProperties==null) {
            try(FileInputStream fis=new FileInputStream("c:\\Formations\\TestIO\\db.properties")) {
                cnxProperties=new Properties();
                cnxProperties.load(fis);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            Class.forName(cnxProperties.getProperty("Driver"));
            cnx=DriverManager.getConnection(cnxProperties.getProperty("Url"),cnxProperties.getProperty("UserDb"),cnxProperties.getProperty("PasswordDb"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
       }
       return cnx;
    }
    
    private static void closeConnection() {
        if(cnx!=null) {
            try {
                cnx.close();
                cnx=null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
