package fr.dawan.formation.enums;

public enum Contrat {
    CDI, CDD, INTERIM, STAGE
}
