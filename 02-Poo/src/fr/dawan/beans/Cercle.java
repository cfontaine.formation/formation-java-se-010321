package fr.dawan.beans;

public class Cercle {

    private Point centre;
    private double rayon = 1.0;

    public Cercle(Point centre, double rayon) {
        this.centre = centre;
        this.rayon = rayon;
    }

    public Point getCentre() {
        return centre;
    }

    public void setCentre(Point centre) {
        this.centre = centre;
    }

    public double getRayon() {
        return rayon;
    }

    public void setRayon(double rayon) {
        this.rayon = rayon;
    }

    public boolean collision(Cercle c) {
        return Point.distance(centre, c.centre) < (rayon + c.rayon);
    }

    public boolean contenu(Point p) {
        return Point.distance(centre, p) < rayon;
    }
}
