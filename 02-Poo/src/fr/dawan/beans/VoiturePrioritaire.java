package fr.dawan.beans;

public class VoiturePrioritaire extends Voiture {
    
    private boolean gyro;

    public VoiturePrioritaire(String marque,boolean gyro) {
        super(marque,0);
        this.gyro = gyro;  
    }
    
    public VoiturePrioritaire(String marque, int vitesse) {
        super(marque, vitesse);
    }

    public VoiturePrioritaire(String marque, String couleur, String plaqueIma, int vitesse) {
        super(marque, couleur, plaqueIma, vitesse);

    }

    public VoiturePrioritaire(boolean gyro) {
        this.gyro = gyro;
    }
    
    public void allumerGyro() {
        gyro=true;
        setMarque("Toyota");
        vitesse+=20;
    }

    public void eteindreGyro() {
        gyro=false;
    }

    @Override
    public void afficher() {
        super.afficher();
        System.out.println("Gyro=" + gyro);
    }

}
