package fr.dawan.beans;

public class CompteEpargne extends CompteBancaire {

    public static final double TAUX_ANNUEL = 0.5;

    private double taux = TAUX_ANNUEL;

    public CompteEpargne(String titulaire, double solde, double taux) {
        super(titulaire, solde);
        this.taux = taux;
    }

    public double getTaux() {
        return taux;
    }

    public void setTaux(double taux) {
        this.taux = taux;
    }

    public void calculInterets() {
        solde *= (1 + taux / 100);
    }

    @Override
    public void afficher() {
        super.afficher();
        System.out.println("taux=" + taux);
    }

}
