package fr.dawan.beans;

public class CompteBancaire {

    protected double solde = 50.0;
    private String iban;
    private String titulaire;

    private static int nbCompte;

    public CompteBancaire() {
        nbCompte++;
        iban = "fr-5900-" + nbCompte;
    }

    public CompteBancaire(String titulaire) {
        this();
        this.titulaire = titulaire;
    }

    public CompteBancaire(String titulaire, double solde) {
        this(titulaire);
        this.solde = solde;
    }

    public String getTitulaire() {
        return titulaire;
    }

    public void setTitulaire(String titulaire) {
        this.titulaire = titulaire;
    }

    public double getSolde() {
        return solde;
    }

    public String getIban() {
        return iban;
    }

    public static int getNbCompte() {
        return nbCompte;
    }

    public void afficher() {
        System.out.println("_____________________");
        System.out.println("Solde= " + solde);
        System.out.println("Iban= " + iban);
        System.out.println("Titulaire= " + titulaire);
        System.out.println("_____________________");
    }

    public void crediter(double valeur) {
        if (valeur > 0.0) {
            solde += valeur;
        }
    }

    public void debiter(double valeur) {
        if (valeur > 0.0) {
            solde -= valeur;
        }
    }

    public boolean estPositif() {
        return solde >= 0.0;
    }
}
