package fr.dawan.beans;

public /*final*/ class Voiture{   // final =>  interdire l'héritage à partir de la classe
    
    // Variable d'instance (attribut)
    
    // L'encapsulation consiste à cacher l'état interne d'un objet et d'imposer de
    // passer par des méthodes permettant un accès sécurisé à l'état de l'objet
    // private => attribut accessible seulement dans la classe elle-même
    private String marque;
    private String couleur="Noir";  // Noir valeur par défaut de l'attribut couleur
    private String plaqueIma;
    protected int vitesse;
    private double niveauCarburant = 5.0; // 5.0 valeur par défaut de l'attribut niveauCarburant
    // Agrégation
    private Personne proprietaire;
    
    // Variable de classe
    private static int cptVoiture;
    
    // Constructeurs
    // constructeur par défaut (sans paramètres)
    public Voiture() {
        cptVoiture++;
    }

    // On peut surcharger le constructeur
    public Voiture(String marque,int vitesse ){
        this();
        this.marque=marque; // utilisation de this pour indiquer que l'on acccède à la variable d'instance
        this.vitesse=vitesse;
    }
    
    public Voiture(String marque, String couleur, String plaqueIma, int vitesse) {
        // Appel d'un autre constructeur avec this, doit être la première instruction du constructeur
        this(marque,vitesse);
        this.couleur = couleur;
        this.plaqueIma = plaqueIma;
    }

    // Getters/Setters
    // Un getter permet l'accès en lecture à un attribut
    public String getCouleur() {
        return couleur;
    }
    
    // Un setter permet de demander un changement d'état
    public void setCouleur(String couleur) {
        this.couleur=couleur;
    }
    
    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getPlaqueIma() {
        return plaqueIma;
    }

    public void setPlaqueIma(String plaqueIma) {
        this.plaqueIma = plaqueIma;
    }

    public double getNiveauCarburant() {
        return niveauCarburant;
    }

    public void setNiveauCarburant(double niveauCarburant) {
        if(niveauCarburant>=0 ) {
            this.niveauCarburant = niveauCarburant;
        }
    }

    public Personne getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Personne proprietaire) {
        this.proprietaire = proprietaire;
    }

    public int getVitesse() {
        return vitesse;
    }

    // Méthodes d'instances
    
    public final void accelerer(int vitesseAcc) { // final => interdire la redéfinition d'une méthode
        if (vitesseAcc > 0) {
            vitesse += vitesseAcc;
        }
    }

    public void freiner(int vitesseFrn) {
        if (vitesseFrn > 0) {
            vitesse -= vitesseFrn;
        }
    }

    public void arreter() {
        vitesse = 0;
    }

    public boolean estArreter() {
        if (vitesse != 0) {
            return false;
        } else {
            return true;
        }
    }
    
    public void afficher() {
        System.out.println("Marque= "+ marque + " couleur= "+ couleur + " Plaque d'imatriculation= " + plaqueIma +" Vitesse= "+ vitesse+ " Niveaux Carburant= "+ niveauCarburant);
    }
    
    // Méthodes de classe
    
    public static int getCptVoiture() {
        return cptVoiture;
    }

    public static void testMethodeClasse() {
        System.out.println("Methode de classe");
        System.out.println(cptVoiture);
        // System.out.println(vitesse); // Erreur=> une méthode de classe ne peut pas accèder à une variable d'instance
        // estArreter();                //          ou à une méthode d'instance

    }
    
    public static boolean equalVitesse(Voiture va,Voiture vb) {
        // Dans un méthode de classe on peut accèder à une variable d'instance
        // si la référence d'un objet est passée en paramètre
        return va.vitesse==vb.vitesse;
    }
}
