package fr.dawan;

import java.util.Date;

import fr.dawan.beans.Cercle;
import fr.dawan.beans.CompteBancaire;
import fr.dawan.beans.CompteEpargne;
import fr.dawan.beans.Point;
import fr.dawan.beans.Voiture;
import fr.dawan.beans.VoiturePrioritaire;

public class Application {

    public static void main(String[] args) {
        System.out.println("Compteur nb voiture" + Voiture.getCptVoiture()); // Voiture.cptVoiture

        // Appel d'une méthode de classe
        Voiture.testMethodeClasse();

        // Instantiation de la classe Voiture
        Voiture v1 = new Voiture("Opel", "Gris", "AB-456-NR", 10);
        System.out.println("Compteur nb voiture" + Voiture.getCptVoiture());

        // Appel d’une méthode d’instance
        v1.afficher();
        // Pour les modifiés les attributs, on doit passer par les setters
        v1.setCouleur("Vert"); // v1.couleur="Vert" n'est plus accessible => privé
        System.out.println(v1.getCouleur());

        v1.accelerer(10);
        v1.afficher();
        v1.freiner(3);
        v1.afficher();
        System.out.println(v1.estArreter());
        v1.arreter();
        System.out.println(v1.estArreter());
        v1.afficher();

        Voiture v2 = new Voiture();
        System.out.println("Compteur nb voiture" + Voiture.getCptVoiture());
        v2.afficher();
        v2.accelerer(10);
        v2.setCouleur("Rouge"); // v2.couleur="Rouge";
        System.out.println(v2.getCouleur()); // v2.couleur

        System.out.println(Voiture.equalVitesse(v1, v2));

        // Compte bancaire
        CompteBancaire cb = new CompteBancaire("John Doe");
        cb.afficher();
        cb.crediter(100.0);
        cb.afficher();

        CompteBancaire cb2 = new CompteBancaire("Jane Doe");
        cb2.afficher();

        // Appel explicite du garbage collector => à eviter
        // System.gc();

        // Point
        Point p1 = new Point();
        p1.afficher();
        p1.translation(0, 1);
        p1.afficher();

        Point p2 = new Point(1, 1);
        System.out.println(Point.distance(p1, p2));

        // Cercle
        Point p3 = new Point(2, 0);
        Cercle c1 = new Cercle(p3, 2.0);
        Cercle c2 = new Cercle(new Point(8, 0), 2.0);
        Cercle c3 = new Cercle(new Point(3, 0), 2.0);

        System.out.println(c1.collision(c2));
        System.out.println(c1.collision(c3));

        System.out.println(c1.contenu(p2));
        p1.setX(10);
        System.out.println(c1.contenu(p1));

        // Package
        Date today = new Date();
        java.sql.Date otherDay;

        // Héritage
        VoiturePrioritaire vp1 = new VoiturePrioritaire("Ford", false);
        vp1.accelerer(10);
        vp1.afficher();

        // Compte épargne
        CompteEpargne ce = new CompteEpargne("Alan Smithee", 400.0, 0.75);
        ce.afficher();
        ce.crediter(75.0);
        ce.afficher();
        ce.calculInterets();
        ce.afficher();
        System.out.println(CompteEpargne.TAUX_ANNUEL);
    }

}
